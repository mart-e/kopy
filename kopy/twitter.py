import logging
import tweepy
from .base import BaseExtractor, Status

logger = logging.getLogger(__name__)


class TwitterExtractor(BaseExtractor):
    def parse_config(self, config):
        auth = tweepy.OAuthHandler(config["consumer_key"], config["consumer_secret"])
        auth.set_access_token(config["access_token_key"], config["access_token_secret"])
        self.api = tweepy.API(auth)
        return True

    def _get_statuses(self, count=10, since=None):
        try:
            return self.api.home_timeline(
                count=count, tweet_mode="extended", max_id=since
            )
        except tweepy.error.RateLimitError as err:
            logger.warning(f"Network error when fetching Twitter: {err}")
            return []

    def format_status(self, status):
        return f"""
<li>
    <strong>{status.user.name}</strong>:
    {status.text}
</li>
"""

    def convert_status(self, status):
        def text_prettify(entry):
            offset = 0
            text = entry.full_text
            entities = [
                dict(e, type=k) for k, sub in entry.entities.items() for e in sub
            ]
            entities += [
                {"type": "return", "indices": [i, i+1]}
                for i, c in enumerate(text)
                if c == "\n"
            ]
            sorted_entities = sorted(entities, key=lambda i: i["indices"][0])
            for entity in sorted_entities:
                start = entity["indices"][0] + offset
                end = entity["indices"][1] + offset

                if entity["type"] == "return":
                    plain = "\n"
                    rich = "<br/>"
                elif entity["type"] == "hashtags":
                    plain = f"#{entity['text']}"
                    rich = f"<a href='https://twitter.com/hashtag/{entity['text']}' rel='nofollow noopener' target='_blank'>#{entity['text']}</a>"
                elif entity["type"] == "urls":
                    plain = entity["url"]
                    rich = f"<a href='{entity['expanded_url']}' rel='nofollow noopener' target='_blank' title='{entity['expanded_url']}'>{entity['display_url']}</a>"
                elif entity["type"] == "media":
                    plain = entity["url"]
                    rich = f"<a href='{entity['expanded_url']}' rel='nofollow noopener' target='_blank' title='{entity['expanded_url']}'>{entity['display_url']}</a>"
                elif entity["type"] == "user_mentions":
                    plain = entry.full_text[entity["indices"][0] : entity["indices"][1]]
                    rich = f"<a href='https://twitter.com/{entity['screen_name']}' rel='nofollow noopener' target='_blank'>@{entity['screen_name']}</a>"
                else:
                    logger.warning("Skip entity type %s", entity["type"])
                    continue

                text = text[:start] + rich + text[end:]
                offset += len(rich) - len(plain)

            return text

        def extract_media(entry):
            medias = []
            entities = entry.entities.get("media", [])
            if hasattr(entry, "extended_entities"):
                entities.extend(entry.extended_entities.get("media", []))
            for entity in entities:
                if entity["type"] in ["photo", "animated_gif"]:
                    media = {
                        "type": "image",
                        "inline": entity["media_url_https"],
                        "url": entity["media_url_https"],
                    }
                    if media not in medias:
                        medias.append(media)
                elif entity["type"] == "video":
                    medias.append(
                        {"type": "video", "inline": entity["url"], "url": entity["url"]}
                    )
                else:
                    logger.warning(f"Ignore twitter media type {entity['type']}")
            return medias

        if hasattr(status, "retweeted_status"):
            r = status.retweeted_status
            full_text = text_prettify(r)

            retweeted_status = Status(
                sid=str(r.id),
                date=r.created_at,
                author=r.user.name,
                author_title="@" + r.user.screen_name,
                author_avatar=r.user.profile_image_url_https,
                author_url=f"https://twitter.com/{r.user.screen_name}",
                content=full_text,
                extractor=self.name,
                extractor_name=self.display_name,
                url=f"https://twitter.com/{r.user.screen_name}/status/{r.id}",
                reply_count=r.in_reply_to_status_id_str and 1 or 0,
                reblog_count=r.retweet_count,
                favorite_count=r.favorite_count,
                medias=extract_media(r),
            )
        else:
            retweeted_status = False

        full_text = text_prettify(status)
        return Status(
            sid=str(status.id),
            date=status.created_at,
            author=status.user.name,
            author_title="@" + status.user.screen_name,
            author_avatar=status.user.profile_image_url_https,
            author_url=f"https://twitter.com/{status.user.screen_name}",
            content=full_text,
            url=f"https://twitter.com/{status.user.screen_name}/status/{status.id}",
            extractor=self.name,
            extractor_name=self.display_name,
            reply_count=status.in_reply_to_status_id_str and 1 or 0,
            reblog_count=status.retweet_count,
            favorite_count=status.favorite_count,
            medias=extract_media(status),
            original_status=retweeted_status,
        )
