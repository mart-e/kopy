import { renderElem } from './dom.js';

export const _isMoreRecent = (first, second) => {
    return first.timestamp > second.timestamp;
};

export const insertActivity = (vList, vNode) => {
    const index = vList.children.forEach( (child, idx) => {
        if (_isMoreRecent(vNode, child)) {
            return idx;
        }
    });
    if (index) {
        vList.children.splice(index, 0, vNode);
    } else {
        vList.children.push(vNode);
    }
};

export const insertActivities = (vList, vActivities) => {
    // remove skeleton
    vList.children = vList.children.filter(e => e.attrs.class === "skeleton");
    vActivities.forEach( vActivity => {
        insertActivity(vList, vActivity);
    });
};

export const attachEvents = ($node) => {
    if ($node.children[2].children.length <= 2) {
        return true;
    }
    const $link = $node.children[2].children[2].children[0];
    if ($link.attributes['class'].value !== 'toggle-media') {
        return true;
    }

    // first is the link then the medias
    const $imgs = Array.from($node.children[2].children[2].children).slice(1);
    $link.addEventListener('click', (event) => {
        event.preventDefault();
        $imgs.forEach( $img => {
            if ($img.style.display === 'none') {
                $img.style.display = "inline-block";
                $link.text = "Hide media";
            } else {
                $img.style.display = "none";
                $link.text = "Show media";
            }
        });
    });
};

export const renderActivityList = ($vList) => {
    const $list = renderElem($vList);
    for (const $activity in $list.children) {
        attachEvents($activity);
    }
    return $list;
}