// abstract methods for vitrual dom
// source https://dev.to/ycmjason/building-a-simple-virtual-dom-from-scratch-3d05

export const createElement = (tagName, { attrs = {}, children = [] }) => {
    // create a virtual element
    const vElem = Object.create(null);

    Object.assign(vElem, {
        tagName,
        attrs,
        children,
    });

    return vElem;
};

export const renderElem = ({ tagName, attrs, children}) => {
    // render the virtual element to an HTML node
    const $el = document.createElement(tagName);

    for (const [k, v] of Object.entries(attrs)) {
        $el.setAttribute(k, v);
    }

    for (const child of children) {
        if (typeof child === 'string') {
            $el.innerHTML = child;
        } else {
            $el.appendChild(renderElem(child));
        }
    }

    return $el;
};

export const patch = ($node, $target) => {
    // insert the node into the target element
    const nodeDate = $node.attributes['data-date'].value;
    let inserted = false;
    for (var child of $target.childNodes.entries()) {
        if (!child[1].attributes) {
            // skeleton element, to be ignored
            continue;
        }
        const childDate = child[1].attributes['data-date'].value;
        if (!childDate) {
            continue;
        }
        if (nodeDate > childDate) {
            $target.insertBefore($node, child[1]);
            inserted = true;
            break;
        }
    }
    if (!inserted) {
        $target.appendChild($node);
    }
};

export const mount = ($node, $target) => {
  $target.replaceWith($node);
  return $node;
};
