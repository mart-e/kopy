import { createElement } from './dom.js';

export const computevNav = (sources) => {
    // compute navigation sidebar
    const vNav = createElement("ul", {
        children: []
    });
    sources.forEach(source => {
        vNav.children.push(
            createElement("li", {
                children: [source['display_name']],
            })
        );
    });
    return vNav;
}

export const computevSkeleton = (date) => {
    // compute fake skeleton elements
    return createElement('article', {
        attrs: {
            class: 'skeleton',
            'data-date': date,
        },
        children: [
            createElement('header', {
                children: [
                    createElement('div', {
                        attrs: {class: "avatar skeleton-content"}
                    }),
                    createElement('div', {
                        attrs: {class: "skeleton-content"}
                    }),
                ]
            }),
            createElement('div', {
                attrs: {class: 'content'},
                children: [
                    createElement('div', {
                        attrs: {class: "skeleton-content"}
                    })
                ]
            }),
            createElement('footer', {
                children: [
                    createElement('div', {
                        attrs: {class: "skeleton-content"}
                    })
                ]
            }),
        ],
    });
};

export const computevArticle = (activity) => {
    // compute article element
    const vArticle = createElement('article', {
        attrs: {
            id: activity['sid'],
            'data-date': activity['timestamp']
        },
        children: [
            createElement('header', {
                children: [
                    createElement('img', {
                        attrs: {
                            src: activity['r_author_avatar'],
                            class: "avatar"
                        }
                    }),
                    createElement('strong', {
                        children: [
                            createElement('a', {
                                attrs: {
                                    href: activity['r_author_url'],
                                    rel: 'nofollow noopener',
                                    target: '_blank',
                                    title: activity['r_author_title']
                                },
                                children: [
                                    activity['r_author']
                                ]
                            })
                        ]
                    })
                ]
            }),
            createElement('div', {
                attrs: {
                    class: 'content'
                },
                children: [
                    activity['r_content']
                ]
            }),
            createElement('footer', {
                children: [
                    createElement('div', {
                        attrs: {class: 'counters'},
                        children: [
                            activity['reblog_count'] + ' ♺ ' + activity['favorite_count'] + ' ☆',
                        ]
                    }),
                    createElement('div', {
                        children: [
                            'On ' + activity['extractor_name'] + ' ',
                            createElement('a', {
                                attrs: {
                                    href: activity['url'],
                                    rel: 'nofollow noopener',
                                    target: '_blank'
                                },
                                children: [
                                    activity['date']
                                ]
                            })
                        ]
                    })
                ]
            })
        ]
    });
    if (activity['is_r']) {
        vArticle.children[0].children[1].children.push(
            createElement('span', {
                attrs: {
                    class: 'activity_info'
                },
                children: [
                    " ♺ by ",
                    createElement('a', {
                        attrs: {
                            href: activity['author_url'],
                            rel: 'nofollow noopener',
                            target: '_blank'
                        },
                        children: [activity['author']]
                    })
                ]
            })
        );
    }

    if (activity['medias'].length) {
        vArticle.children[2].children.push(
            createElement('div', {
                children: [
                    createElement('a', {
                        attrs: {
                            href: '#',
                            class: 'toggle-media',
                        },
                        children: ["Show medias"]
                    })
                ]
            })
        );
        activity['medias'].forEach( media => {
            const $mediaLink = createElement('a', {
                attrs: {
                    style: 'display:none;',
                    href: media['url'],
                    rel: 'nofollow noopener',
                    target: '_blank'
                },
                children: []
            });
            if (media['type'] == 'image') {
                $mediaLink.children.push(
                    createElement('img', {
                        attrs: {
                            src: media['inline']
                        }
                    })
                );
            } else if (media['type'] == 'video') {
                $mediaLink.children.push(
                    createElement('video', {
                        attrs: {
                            src: media['inline'],
                            controls: ''
                        }
                    })
                );
            }
            vArticle.children[2].children[2].children.push($mediaLink);
        });
    }

    return vArticle;
};

export const computevFirstBorder = (activity) => {
    return createElement('article', {
        attrs: {
            id: "first-" + activity.extractor,
            'data-date': activity.timestamp + 1,
            style: "display:none",
        },
        children: [
            createElement('a', {
                attrs: {
                    href: '#',
                    title: "Load more recent activities on " + activity.extractor
                },
                children: [
                    "⌃ " + activity.extractor
                ]
            })
        ]
    });
};

export const computevLastBorder = (activity) => {
    return createElement('article', {
        attrs: {
            id: "last-" + activity.extractor,
            'data-date': activity.timestamp - 1,
            // style: "display:none",
        },
        children: [
            createElement('a', {
                attrs: {
                    href: '#',
                    title: "Load older activities on " + activity.extractor
                },
                children: [
                    "⌄ " + activity.extractor
                ]
            })
        ]
    });
};

export const computevArticleList = () => {
    return createElement('section', {
        attrs: {id: "article-list"}
    });
}
