import { renderElem, patch, mount } from './dom.js';
import { computevNav, computevSkeleton, computevArticle, computevArticleList } from './compute.js';
import { _isMoreRecent, insertActivity, insertActivities, attachEvents, renderActivityList } from './utils.js';

(() => {

const initNavbar = async () => {
    const res = await fetch("/configuration");
    const sources = await res.json();
    const vNav = computevNav(sources);
    const $nav = renderElem(vNav);
    document.getElementById('nav-bar').appendChild($nav);
}

const removeSkeleton = ($content) => {
    $content.querySelectorAll('.skeleton').forEach( child => {
        child.remove();
    });
};


let upGaps = {};
let downGaps = {};

// const renderBorders = ($articleList) => {
//     for (let extractor in upGaps) {
//         const vFirst = computevFirstBorder(upGaps[extractor]);
//         const $first = renderElem(vFirst);
//         let $activityDom = document.getElementById("first-"+extractor);
//         if (!$activityDom || $activityDom.id !== $first.id) {
//             if ($activityDom)
//                 $activityDom.remove();
//             patch($first, $articleList);
//         }

//         const vLast = computevLastBorder(downGaps[extractor]);
//         const $last = renderElem(vLast);
//         $activityDom = document.getElementById("last-"+extractor);
//         if (!$activityDom || $activityDom.id !== $last.id) {
//             if ($activityDom)
//                 $activityDom.remove();
//             patch($last, $articleList);
//         }
//     }

// };

const fetchPreviousActivities = async (count) => {
    let minStatus = false;
    for (const extractor in downGaps) {
        // fetch only the most out of date first
        if (!minStatus || _isMoreRecent(downGaps[extractor], minStatus)) {
            minStatus = downGaps[extractor];
        }
    }

    const minId = downGaps[minStatus.extractor].sid;
    const resp = await fetch(`/fetch-previous/${minStatus.extractor}/${count}/${minId}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const activities = await resp.json();
    processFetchedActivities(activities);
    // update the minStatus with the new fetched minimum
    minStatus = downGaps[minStatus.extractor];

    for (const extractor in downGaps) {
        if (minStatus.extractor === extractor) {
            continue;
        }

        if (_isMoreRecent(minStatus, downGaps[extractor])) {
            // `minStatus.extractor` still did not catched up with `extractor`
            continue;
        }

        const minId = downGaps[extractor].sid;
        fetch(`/fetch-previous/${extractor}/${count}/${minId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((resp) => {
            return resp.json();
        }).then((activities) => {
            processFetchedActivities(activities);
        });
    }
};

const fetchActivities = async (count) => {
    const resp = await fetch('/fetch/' + count, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    });

    return processFetchedActivities(await resp.json());
};

const processFetchedActivities = (activities) => {
    let maxActivities = {};
    let minActivities = {};
    const $articleList = document.getElementById('article-list');
    removeSkeleton($articleList);

    const vActivites = [];
    activities.forEach(activity => {
        const extr = activity.extractor;

        if (!(extr in maxActivities) || _isMoreRecent(activity, maxActivities[extr])) {
            // initialise border activities
            maxActivities[extr] = activity;
        }
        if (!(extr in minActivities) || _isMoreRecent(minActivities[extr], activity)) {
            // initialise border activities
            minActivities[extr] = activity;
        }

        const $activityDom = document.getElementById(activity['sid']);
        if (!$activityDom) {

            const vArticle = computevArticle(activity);
            vActivites.push(vArticle);
            const $article = renderElem(vArticle);
            attachEvents($article);
            patch($article, $articleList);

        }
    });
    for (const extractor in maxActivities) {
        if (!(extractor in upGaps) || _isMoreRecent(maxActivities[extractor], upGaps[extractor])) {
            upGaps[extractor] = maxActivities[extractor];
        }
    }
    for (const extractor in minActivities) {
        if (!(extractor in downGaps) || _isMoreRecent(downGaps[extractor], minActivities[extractor])) {
            downGaps[extractor] = minActivities[extractor];
        }
    }
    // renderBorders($articleList);
    return vActivites;
};

document.addEventListener("DOMContentLoaded", function() {

    initNavbar()
    const vArticleList = computevArticleList();
    document.addEventListener('keydown', (event) => {
        const keyName = event.key;
        if (keyName == 'Home') {
            const vSkeleton = computevSkeleton(9999999999);
            insertActivity(vArticleList, vSkeleton);
            mount(renderElem(vArticleList), document.getElementById('article-list'));
            // patch(renderElem(vSkeleton), document.getElementById('article-list'));
            fetchActivities(30).then( vActivities => {
                insertActivities(vArticleList, vActivities);
                mount(renderActivityList(vArticleList), document.getElementById('article-list'));
            });
        } else if (keyName == 'End') {
            const vSkeleton = computevSkeleton(1);
            patch(renderElem(vSkeleton), document.getElementById('article-list'));
            fetchPreviousActivities(30);
        }
    });

    for (const x of Array(5).keys()) {
        vArticleList.children.push(computevSkeleton(9999999999 - x));
    }
    // set skeleton while waiting the slow fetchActivities
    mount(renderElem(vArticleList), document.getElementById('article-list'));

    fetchActivities(40).then( vActivities => {
        vArticleList.children = vActivities;
        mount(renderActivityList(vArticleList), document.getElementById('article-list'));
    });

});

})();
